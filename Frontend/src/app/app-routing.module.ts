import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes =
  [
    { path: '', loadChildren: () => import('./pages/order/order.module').then(m => m.OrderModule) },
    { path: ':id/details', loadChildren: () => import('./pages/order/order.module').then(m => m.OrderModule) },
    { path: 'new', loadChildren: () => import('./pages/order/order.module').then(m => m.OrderModule) }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
