import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { IOrder } from 'src/app/interfaces/iorder';
import { OrderService } from 'src/app/services/order.service';
import { SnackBarService } from 'src/app/services/snackbar.service';
@Component({
  selector: 'app-order-listing',
  templateUrl: './order-listing.component.html',
  styleUrls: ['./order-listing.component.scss']
})
export class OrderListingComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['orderId', 'date', 'customer', 'status', 'view', 'edit', 'delete'];
  orders: MatTableDataSource<IOrder>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private orderService: OrderService,
    private router: Router,
    private snackBar: SnackBarService) { }

  ngAfterViewInit(): void {

  }

  ngOnInit(): void {
    this.getAllOrders();
  }

  onBackClick() {
    this.router.navigate(['/']);
  }

  getAllOrders() {
    this.orderService.getAllOrders().subscribe(orders => {
      this.orders = new MatTableDataSource(orders);
      this.orders.paginator = this.paginator;
      this.sort.sortChange.subscribe(sort => {
        if (sort.direction === 'desc') {
          this.orders.data.sort((a, b) => (a.customerName > b.customerName ? -1 : 1))
        } else {
          this.orders.data.sort((a, b) => a.customerName < b.customerName ? -1 : a.customerName > b.customerName ? 1 : 0)
        }
        this.orders.sort = this.sort;

      });
    }, () => {

    });
  }

  onDelete(id) {
    this.orderService.deleteOrder(id).subscribe(() => {
      this.getAllOrders();
      this.snackBar.openSaveSnackbar(true);
    });
  }

}
