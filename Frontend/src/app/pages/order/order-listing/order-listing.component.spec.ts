import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppModule } from 'src/app/app.module';

import { OrderListingComponent } from './order-listing.component';
import { OrderModule } from '../order.module';

describe('OrderListingComponent', () => {
  let component: OrderListingComponent;
  let fixture: ComponentFixture<OrderListingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderListingComponent ],
      imports:[OrderModule, AppModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
