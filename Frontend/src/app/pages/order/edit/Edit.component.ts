import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IOrder } from 'src/app/interfaces/iorder';
import { OrderService } from 'src/app/services/order.service';
import { FormGroup, FormControl } from '@angular/forms'
import { ItemService } from 'src/app/services/item.service';
import { IITerm } from 'src/app/interfaces/iitem';
import { MatTableDataSource } from '@angular/material/table';
import { DialogService } from 'src/app/services/dialog-service';
import { CustomerService } from 'src/app/services/customer.service';
import { ICustomer } from 'src/app/interfaces/icustomer';
import { IUpdateOrderRequest } from 'src/app/interfaces/iupdate-order-request';
import { SnackBarService } from 'src/app/services/snackbar.service';

@Component({
  selector: 'app-details',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  panelOpenState = true;
  isEditClicked: boolean = false;
  order: IOrder;
  orderItems: IITerm[];
  userForm: FormGroup;
  orderForm: FormGroup;
  documentsForm: FormGroup;
  displayedColumns: string[] = ['product', 'cost', 'quantity', 'delete'];
  dataSource: MatTableDataSource<IITerm>;
  orderId: string;
  docsForm: FormGroup;

  constructor(private activeRouter: ActivatedRoute,
    private router: Router,
    private orderService: OrderService,
    private itemService: ItemService,
    private dialogService: DialogService,
    private customerService: CustomerService,
    private snackbarService: SnackBarService) { }

  ngOnInit(): void {
    this.createUserForm();
    this.createOrderForm();
    this.createDocumentsForm();
    this.activeRouter.params.subscribe(param => {
      this.getOrderDetails(param.id);
      this.orderId = param.id;
    });
  }

  onRemoveItem(id) {
    this.itemService.deleteItem(id).subscribe(() => {
      this.getOrderItemsByOrderId(this.orderId);
    }, () => {

    })
  }

  getOrderDetails(orderId: string) {
    this.orderService.getOrder(orderId).subscribe(order => {
      this.order = order;
      this.getOrderItemsByOrderId(orderId);
      this.createUserForm();
    }, () => {

    });
  }

  getOrderItemsByOrderId(orderId: string) {
    this.itemService.getItemsByOrderId(orderId).subscribe(items => {
      this.orderItems = items;
      this.createOrderForm();
      this.dataSource = new MatTableDataSource(items);
    }, () => {

    })
  }

  onUpdateCustomer() {
    const name = this.userForm.controls?.name.value;
    const address = this.userForm.controls?.address.value;
    const customer: ICustomer = { id: this.order.customerId, name: name, address: address, phone: '' };
    this.customerService.updateCustomer(customer).subscribe(() => {
      this.snackbarService.openSaveSnackbar(true);
    }, () => {
      this.snackbarService.openSaveSnackbar(false);
    })
  }

  onUpdateOrder() {
    const orderStatus = this.orderForm.controls.status?.value;
    const order: IUpdateOrderRequest = {
      id: this.orderId, status: orderStatus,
      purchaseOrder: this.order.purchaseOrder,
      invoice: this.order.invoice,
      deliveryNote: this.order.deliveryNote
    };
    this.orderService.updateOrder(order).subscribe(() => {
      this.snackbarService.openSaveSnackbar(true);
    }, () => {
      this.snackbarService.openSaveSnackbar(false);
    });
  }

  onAddClick() {
    const dialog = this.dialogService.openDialog({ orderId: this.orderId });
    dialog.afterClosed().subscribe(() => {
      this.getOrderItemsByOrderId(this.orderId);
    })
  }

  onBackClick() {
    this.router.navigate(['/'])
  }

  onInputChange(evt, index) {
    var tgt = evt.target || window.event.srcElement,
      files = tgt.files;
    let docs = this.docsForm;
    const order = this.order;
    if (FileReader && files && files.length && files[0].type==='application/pdf') {
      var fr = new FileReader();
      fr.onload = function () {
        var base64 = fr.result;
        if (index === 0) {
          docs.controls.purchaseOrder.setValue(base64);
          order.purchaseOrder = docs.controls.purchaseOrder.value;
        } else if (index === 1) {
          docs.controls.invoice.setValue(base64);
          order.invoice = docs.controls.invoice.value;
        } else {
          docs.controls.deliveryNote.setValue(base64);
          order.deliveryNote = docs.controls.deliveryNote.value;
        }

      }
      fr.readAsDataURL(files[0]);
    }else{
      this.snackbarService.openSnackbar('Pleas upload a PDF file','error-snackbar');
    }
  }


  createUserForm() {
    this.userForm = new FormGroup({
      name: new FormControl(this.order?.customerName),
      address: new FormControl(this.order?.address),
    });
  }

  createOrderForm() {
    this.orderForm = new FormGroup({
      status: new FormControl(this.order?.status),
      date: new FormControl(this.order?.orderDate),
    })
  }

  createDocumentsForm() {
    this.documentsForm = new FormGroup({
      purchaseOrder: new FormControl(''),
      deliveryNote: new FormControl(''),
      invoice: new FormControl('')
    });

    this.docsForm = new FormGroup({
      purchaseOrder: new FormControl(''),
      deliveryNote: new FormControl(''),
      invoice: new FormControl('')
    })
  }
}
