import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';
import { OrderRoutingModule } from './order-routing.module';
import { OrderListingComponent } from './order-listing/order-listing.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatDividerModule } from '@angular/material/divider';
import { EditComponent } from './edit/Edit.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatListModule } from '@angular/material/list';
import { MatDialogModule } from '@angular/material/dialog';
import { NewOrderComponent } from './new-order/new-order.component';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { MatStepperModule } from '@angular/material/stepper';
import { SafePipeModule } from 'safe-pipe';
import { DocsComponent } from './docs/docs.component';
import { MatSortModule } from '@angular/material/sort';
@NgModule({
  declarations: [
    OrderListingComponent,
    EditComponent,
    NewOrderComponent,
    OrderDetailsComponent,
    DocsComponent
  ],
  imports: [
    CommonModule,
    OrderRoutingModule,
    FlexLayoutModule,
    MatCardModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    RouterModule,
    MatPaginatorModule,
    MatDividerModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    ReactiveFormsModule,
    MatExpansionModule,
    MatListModule,
    MatDialogModule,
    MatStepperModule,
    SafePipeModule,
    MatSortModule,
    FormsModule,
    ReactiveFormsModule
  ], exports: [MatSortModule]
})
export class OrderModule { }
