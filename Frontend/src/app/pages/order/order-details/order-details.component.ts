import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { IITerm } from 'src/app/interfaces/iitem';
import { IOrder } from 'src/app/interfaces/iorder';
import { ItemService } from 'src/app/services/item.service';
import { OrderService } from 'src/app/services/order.service';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})
export class OrderDetailsComponent implements OnInit {

  panelOpenState = true;
  isEditClicked: boolean = false;
  order: IOrder;
  orderItems: IITerm[];
  userForm: FormGroup;
  orderForm: FormGroup;
  displayedColumns: string[] = ['product', 'cost', 'quantity'];
  dataSource: MatTableDataSource<IITerm>;
  orderId: string;

  constructor(private activeRouter: ActivatedRoute,
    private router: Router,
    private orderService: OrderService,
    private itemService: ItemService) { }

  ngOnInit(): void {
    this.createUserForm();
    this.createOrderForm();
    this.activeRouter.params.subscribe(param => {
      this.getOrderDetails(param.id);
      this.orderId = param.id;
    });
  }

  onRemoveItem(id) {
    this.itemService.deleteItem(id).subscribe(() => {
      this.getOrderItemsByOrderId(this.orderId);
    }, () => {

    })
  }

  getOrderDetails(orderId: string) {
    this.orderService.getOrder(orderId).subscribe(order => {
      this.order = order;
      this.getOrderItemsByOrderId(orderId);
      this.createUserForm();
    }, () => {

    });
  }

  getOrderItemsByOrderId(orderId: string) {
    this.itemService.getItemsByOrderId(orderId).subscribe(items => {
      this.orderItems = items;
      this.createOrderForm();
      this.dataSource = new MatTableDataSource(items);
    }, () => {

    })
  }

  onBackClick() {
    this.router.navigate(['/'])
  }

  createUserForm() {
    this.userForm = new FormGroup({
      name: new FormControl({ value: this.order?.customerName, disabled: true }),
      address: new FormControl({ value: this.order?.address, disabled: true }),
    });
  }

  createOrderForm() {
    this.orderForm = new FormGroup({
      status: new FormControl({ value: this.order?.status, disabled: true }),
      date: new FormControl({value:this.order?.orderDate, disabled:this.order?.orderDate}),
    })
  }
}
