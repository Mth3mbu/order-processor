import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-docs',
  templateUrl: './docs.component.html',
  styleUrls: ['./docs.component.scss']
})
export class DocsComponent implements OnInit {
  @Input() invoice;
  @Input() deliveryNote;
  @Input() purchaseOrder;

  constructor() { }

  ngOnInit(): void {

  }

}

