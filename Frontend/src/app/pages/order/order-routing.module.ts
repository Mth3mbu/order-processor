import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditComponent } from './edit/Edit.component';
import { NewOrderComponent } from './new-order/new-order.component';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { OrderListingComponent } from './order-listing/order-listing.component';

const routes: Routes = [{ path: '', component: OrderListingComponent },
{ path: ':id/details', component: EditComponent },
{ path: 'new', component: NewOrderComponent },
{ path: ':id',  component: OrderDetailsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderRoutingModule { }
