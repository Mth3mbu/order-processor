import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { CustomerService } from 'src/app/services/customer.service';
import { FormService } from 'src/app/services/form.service';
import { ItemService } from 'src/app/services/item.service';
import { OrderService } from 'src/app/services/order.service';
import { SnackBarService } from 'src/app/services/snackbar.service';

@Component({
  selector: 'app-new-order',
  templateUrl: './new-order.component.html',
  styleUrls: ['./new-order.component.scss']
})
export class NewOrderComponent implements OnInit {

  customerForm: FormGroup;
  orderForm: FormGroup;
  orderFormGroup: FormGroup;
  orderItemsFormGroup: FormGroup;
  itemsFormArray: FormArray;
  form: FormGroup;

  constructor(private orderServie: OrderService,
    private formService: FormService,
    private customerService: CustomerService,
    private itemService: ItemService,
    private snackBarService: SnackBarService,
    private router: Router) { }

  get items(): FormArray {
    return this.form.get('items') as FormArray;
  }

  ngOnInit() {
    this.orderFormGroup = this.formService.getUpdatedOrderForm();
    this.customerForm = this.formService.getCustomerForm();
    this.orderForm = this.formService.getOrderForm(this.customerForm.controls.id.value);
    this.orderItemsFormGroup = this.formService.getItemsFormGroup(this.orderFormGroup.controls.id.value);
    this.form = this.formService.getItemsFormArray(this.orderItemsFormGroup);

    this.creatItemFormArray();
  }

  onMoreClick() {
    this.addFormGroup();
  }

  addFormGroup() {
    const formArray = this.form.get('items') as FormArray;
    const length = formArray.length;
    formArray.insert(length, this.formService.getItemsFormGroup(this.orderFormGroup.controls.id.value));
  }

  removeFormGroup(index: number) {
    const formArray = this.form.get('items') as FormArray;
    formArray.removeAt(index);
  }

  creatItemFormArray() {
    this.itemsFormArray = new FormArray([
      this.orderItemsFormGroup
    ]);
  }

  onstatusChange(event) {
    this.orderFormGroup.controls.status.setValue(event.value)
    this.orderFormGroup.controls.customerId.setValue(this.customerForm.controls.id.value);
  }

  onOrderItemNextClick() {
    this.orderItemsFormGroup?.controls?.orderId?.setValue(this.orderFormGroup?.controls?.id?.value);
    this.orderFormGroup?.controls?.itemId.setValue(this.orderItemsFormGroup?.controls?.id.value);
  }

  onPlaceOrder() {
    this.addCustommer();
  }

  addCustommer() {
    this.customerService.addCustomer(this.customerForm.value).subscribe(() => {
      this.addOrder();
    }, () => {

    })
  }

  addOrder() {
    this.orderServie.placeOrder(this.orderFormGroup.value).subscribe(() => {
      this.addItems();
    }, () => {

    })
  }

  addItems() {
    this.itemService.addMany(this.items.value).subscribe(() => {
      this.snackBarService.openSaveSnackbar(true);
      this.router.navigate(['/']);
    })
  }

  onInputChange(evt, index) {
    var tgt = evt.target || window.event.srcElement,
      files = tgt.files;
    let orderForm = this.orderFormGroup;
    if (FileReader && files && files.length && files[0].type === 'application/pdf') {
      var fr = new FileReader();
      fr.onload = function () {
        var base64 = fr.result;
        if (index === 0) {
          orderForm.controls.purchaseOrder.setValue(base64 ?? '');
        } else if (index === 1) {
          orderForm.controls.invoice.setValue(base64 ?? '');
        } else {
          orderForm.controls.deliveryNote.setValue(base64 ?? '');
        }
      }
      fr.readAsDataURL(files[0]);
    } else {
      this.snackBarService.openSnackbar('Pleas upload a PDF file', 'error-snackbar');
    }
  }
}
