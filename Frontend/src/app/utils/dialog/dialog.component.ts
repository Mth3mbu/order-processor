import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Guid } from 'guid-typescript';
import { IITerm } from 'src/app/interfaces/iitem';
import { ItemService } from 'src/app/services/item.service';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {

  name: string;
  cost: number;
  quantity: number;

  constructor(public dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IITerm,
    private itermService: ItemService) { }

  ngOnInit(): void {
  }

  onCancel(): void {
    this.data.cost = this.cost;
    this.data.name = "canceled";
    this.data.quantity = this.quantity;
    this.dialogRef.close();
  }

  onSave(): void {
    this.data.cost = this.cost;
    this.data.name = this.name;
    this.data.quantity = this.quantity;
    this.data.id = Guid.create().toString()
    this.itermService.addItem(this.data).subscribe(response => {
      this.dialogRef.close();
    }, error => {

    })
  }
}
