export interface IOrder {
  id: string;
  customerId: string;
  status: string;
  customerName: string;
  orderDate: string;
  address: string;
  phone: string;
  invoice:string;
  purchaseOrder:string;
  deliveryNote:string;
}
