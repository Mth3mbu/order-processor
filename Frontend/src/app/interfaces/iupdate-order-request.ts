export interface IUpdateOrderRequest {
  id: string;
  status: string;
  invoice:string;
  purchaseOrder:string;
  deliveryNote:string;
}
