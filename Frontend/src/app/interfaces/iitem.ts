export interface IITerm {
  id: string;
  orderId: string;
  name: string;
  quantity: number;
  cost: number;
}
