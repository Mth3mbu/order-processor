import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { IOrder } from '../interfaces/iorder';
import { IUpdateOrderRequest } from '../interfaces/iupdate-order-request';


@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private httpClient: HttpClient) { }

  placeOrder(payload) {
    return this.httpClient.post(`${environment.baseUrl}/place-order`, payload);
  }
  getAllOrders() {
    return this.httpClient.get<IOrder[]>(`${environment.baseUrl}/orders`);
  }

  getOrder(orderId: string) {
    return this.httpClient.get<IOrder>(`${environment.baseUrl}/order/${orderId}`)
  }

  updateOrder(payload: IUpdateOrderRequest) {
    return this.httpClient.put(`${environment.baseUrl}/edit-order`, payload);
  }

  deleteOrder(orderId) {
    return this.httpClient.put(`${environment.baseUrl}/delete-order`, { id: orderId });
  }
}
