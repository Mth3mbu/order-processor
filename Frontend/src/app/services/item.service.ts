import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { IITerm } from '../interfaces/iitem';

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  constructor(private httpClient: HttpClient) { }

  getItemsByOrderId(orderId: string) {
    return this.httpClient.get<IITerm[]>(`${environment.baseUrl}/${orderId}/items`);
  }

  deleteItem(itemId) {
    return this.httpClient.delete(`${environment.baseUrl}/delete/${itemId}`);
  }

  addItem(item: IITerm) {
    return this.httpClient.post(`${environment.baseUrl}/add`, item);
  }

  addMany(items: IITerm[]) {
    return this.httpClient.post(`${environment.baseUrl}/add/many`, items);
  }
}
