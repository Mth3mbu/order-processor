import { Injectable } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { IITerm } from "../interfaces/iitem";
import { DialogComponent } from "../utils/dialog/dialog.component";

@Injectable({
  providedIn: 'root'
})

export class DialogService {
  constructor(private dialog: MatDialog) { }

  openDialog(data) {
      const dialogRef = this.dialog.open(DialogComponent, {
          width: '50em',
          data: data
      });
    return dialogRef;
  }

  openStatusDialog(data:IITerm){
      const dialogRef = this.dialog.open(DialogComponent, {
          width: '250px',
          data:data
        });

   return dialogRef;
  }
}
