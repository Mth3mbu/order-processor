import { TestBed } from '@angular/core/testing';
import { AppModule } from '../app.module';

import { OrderService } from './order.service';

describe('UserService', () => {
  let service: OrderService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppModule]
    });
    service = TestBed.inject(OrderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
