import { Injectable } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Guid } from 'guid-typescript';

@Injectable({
  providedIn: 'root'
})
export class FormService {

  constructor(private formBuilder: FormBuilder) { }

  getCustomerForm() {
    return this.formBuilder.group({
      id: Guid.create().toJSON().value,
      name: ['', Validators.required],
      address: ['', Validators.required],
      phone: ['', Validators.required]
    });
  }

  getOrderForm(customerId) {
    return this.formBuilder.group({
      id: Guid.create().toJSON().value,
      orderDate: [new Date().getDate()],
      status: ['', Validators.required],
      isActive: [1, Validators.required],
      customerId: [customerId],
      purchaseOrder: [''],
      invoice: [''],
      deliveryNote: ['']
    });
  }

  getItemsFormGroup(orderId) {
    return this.formBuilder.group({
      id: new FormControl(Guid.create().toJSON().value),
      orderId: orderId,
      name: new FormControl('', Validators.required),
      cost: new FormControl('', Validators.required),
      quantity: new FormControl('', Validators.required)
    });
  }

  getUpdatedOrderForm() {
    return this.formBuilder.group({
      id: Guid.create().toJSON().value,
      customerId: '',
      orderDate: [new Date().getDate()],
      status: '',
      isActive: 1,
      purchaseOrder: '',
      invoice: '',
      deliveryNote: '',
      itemId: ''
    });
  }

   getItemsFormArray(orderItemsForm: FormGroup) {
    return this.formBuilder.group({
      items: this.formBuilder.array([orderItemsForm])
    });
  }

}
