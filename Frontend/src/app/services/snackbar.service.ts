import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackBarComponent } from '../utils/snack-bar/snack-bar.component';


@Injectable({
    providedIn: 'root'
})
export class SnackBarService {

    constructor(private snackBar: MatSnackBar) { }

    openSaveSnackbar(success: boolean) {
        if (success) {
            this.snackBar.openFromComponent(
                SnackBarComponent, {
                    data: 'Success',
                    duration: 5000,
                    panelClass: 'success-snackbar'
                });
        } else {
            this.snackBar.openFromComponent(
                SnackBarComponent, {
                    data: 'An Error Occurred',
                    duration: 5000,
                    panelClass: 'error-snackbar'
                });
        }
    }

    openSnackbar(message: string, cssClass: string) {
        this.snackBar.openFromComponent(
            SnackBarComponent,
            {
                data: message,
                duration: 6000,
                panelClass: cssClass
            });
    }
}
