import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ICustomer } from '../interfaces/icustomer';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private httpClient: HttpClient) { }

  addCustomer(customer: ICustomer) {
    return this.httpClient.post(`${environment.baseUrl}/customer/new`, customer);
  }

  updateCustomer(customer: ICustomer) {
    return this.httpClient.put(`${environment.baseUrl}/customer`, customer);
  }
}
