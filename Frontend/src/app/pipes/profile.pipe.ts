import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: 'profile',
})
export class ProfilePipe implements PipeTransform {
  transform(value: string) {
    return value.length > 0 ? value.substr(0, 1) : value;
  }
}
