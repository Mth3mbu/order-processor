import { IUser } from "src/app/interfaces/iuser";
import * as faker from 'faker/locale/en';
import { TestDataBuilder } from "./test-data-builder";

export class UserTestDataBuilder extends TestDataBuilder<UserTestDataBuilder, IUser> {
  constructor() {
    super(() => {
      return {} as IUser;
    });
  }
  static create() {
    return new UserTestDataBuilder();
  }

  withId(id: number) {
    return this.withProp(o => o.id = id);
  }

  withName(name: string) {
    return this.withProp(o => o.name = name);
  }

  withLastName(lastName: string) {
    return this.withProp(o => o.lastName = lastName);
  }

  withCountry(country: string) {
    return this.withProp(o => o.country = country);
  }

  withCity(city: string) {
    return this.withProp(o => o.city = city);
  }

  withProvince(province: string) {
    return this.withProp(o => o.province = province);
  }

  withStreet(street: string) {
    return this.withProp(o => o.street = street)
  }
  withPostCode(code: number) {
    return this.withProp(o => o.postcode = code)
  }

  withRandomProps() {
    return this.withId(faker.random.number())
      .withName(faker.name.findName())
      .withLastName(faker.name.lastName())
      .withCountry(faker.address.county())
      .withProvince(faker.random.word())
      .withCity(faker.address.city())
      .withStreet(faker.address.streetName())
      .withPostCode(faker.random.number())
  }
}
