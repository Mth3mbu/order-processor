// import { isFakeMousedownFromScreenReader } from "@angular/cdk/a11y";
// import { of } from "rxjs";
// import { IUser } from "src/app/interfaces/iuser";
// import { UserService } from "src/app/services/order.service";
// import { TestDataBuilder } from "./test-data-builder";
// import { UserTestDataBuilder } from "./user-test-data-builder";

// export class UserServiceTestDataBuilder extends TestDataBuilder<UserServiceTestDataBuilder, jasmine.SpyObj<UserService>> {
//   constructor() {
//     super(() => {
//       return jasmine.createSpyObj<UserService>(['getAllUsers', 'getOrdersByUserId']);
//     });
//   }

//   static create() {
//     return new UserServiceTestDataBuilder();
//   }

//   withGetAllUsers(users?: IUser[]) {
//     const expectedValue = users || UserTestDataBuilder.create().buildList(10);
//     return this.withProp(o => o.getAllUsers.and.returnValue(of(expectedValue)))
//   }

//   withRandomProps() {
//     return this;
//   }
// }
