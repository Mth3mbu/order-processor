﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Sales.Domain.Orders;
using Sales.Domain.Output;

namespace Sales.Domain.Items.UseCases
{
    public class GetItemsUseCase : IGetITermsUseCase
    {
        private readonly IItemsGateway _itemsGateway;

        public GetItemsUseCase(IItemsGateway itemsGateway)
        {
            _itemsGateway = itemsGateway;
        }

        public async Task Execute(Guid orderId, ISuccessOrErrorActionResultPresenter<List<Item>, ErrorDto> presenter)
        {
            presenter.Success(await _itemsGateway.GetAllItemsByOrderId(orderId));
        }
    }
}
