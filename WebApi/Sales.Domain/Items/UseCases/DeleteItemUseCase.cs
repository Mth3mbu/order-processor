﻿using System;
using System.Threading.Tasks;
using Sales.Domain.Orders;
using Sales.Domain.Output;

namespace Sales.Domain.Items.UseCases
{
    public class DeleteItemUseCase : IDeleteItemUseCase
    {
        private readonly IItemsGateway _itemsGateway;

        public DeleteItemUseCase(IItemsGateway itemsGateway)
        {
            _itemsGateway = itemsGateway;
        }

        public async Task Execute(Guid orderId, IErrorActionResultPresenter<ErrorDto> presenter)
        {
            await _itemsGateway.Delete(orderId);
        }
    }
}
