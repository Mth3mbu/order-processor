﻿using System;
using System.Threading.Tasks;
using Sales.Domain.Output;

namespace Sales.Domain.Items.UseCases
{
    public interface IDeleteItemUseCase
    {
        Task Execute(Guid orderId, IErrorActionResultPresenter<ErrorDto> presenter);
    }
}
