﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Sales.Domain.Orders;
using Sales.Domain.Output;

namespace Sales.Domain.Items.UseCases
{
    public interface IGetITermsUseCase
    {
        Task Execute(Guid orderId, ISuccessOrErrorActionResultPresenter<List<Item>, ErrorDto> presenter);
    }
}
