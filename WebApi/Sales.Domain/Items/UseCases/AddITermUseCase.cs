﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Sales.Domain.Orders;
using Sales.Domain.Output;

namespace Sales.Domain.Items.UseCases
{
    public class AddITermUseCase : IAddItemUseCase
    {
        private readonly IItemsGateway _itemsGateway;

        public AddITermUseCase(IItemsGateway itemsGateway)
        {
            _itemsGateway = itemsGateway;
        }

        public async Task Execute(Item request, IErrorActionResultPresenter<ErrorDto> presenter)
        {
            await _itemsGateway.Add(request);
        }

        public void ExecuteMany(IEnumerable<Item> request, IErrorActionResultPresenter<ErrorDto> presenter)
        {
            _itemsGateway.AddMany(request);
        }
    }
}
