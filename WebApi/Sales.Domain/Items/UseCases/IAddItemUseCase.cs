﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Sales.Domain.Orders;
using Sales.Domain.Output;

namespace Sales.Domain.Items.UseCases
{
    public interface IAddItemUseCase
    {
        Task Execute(Item request, IErrorActionResultPresenter<ErrorDto> presenter);
        void ExecuteMany(IEnumerable<Item> request, IErrorActionResultPresenter<ErrorDto> presenter);
    }
}
