﻿using System.Threading.Tasks;

namespace Sales.Domain.Customer
{
    public interface ICustomerGateway
    {
        Task Add(Customer customer);
        Task Update(Customer customer);
    }
}
