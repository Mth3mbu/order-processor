﻿using System.Threading.Tasks;
using Sales.Domain.Output;

namespace Sales.Domain.Customer.CustomerUseCase
{
    public  interface IUpdateCustomerUseCase
    {
        Task Execute(Customer request, IErrorActionResultPresenter<ErrorDto> presenter);
    }
}
