﻿using System.Threading.Tasks;
using Sales.Domain.Output;

namespace Sales.Domain.Customer.CustomerUseCase
{
    public interface IAddCustomerUseCase
    {
        Task Execute(Customer customer, IErrorActionResultPresenter<ErrorDto> presenter);
    }
}
