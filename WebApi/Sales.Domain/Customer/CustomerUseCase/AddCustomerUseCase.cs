﻿using System.Threading.Tasks;
using Sales.Domain.Output;

namespace Sales.Domain.Customer.CustomerUseCase
{
    public class AddCustomerUseCase : IAddCustomerUseCase
    {
        private readonly ICustomerGateway _customerGateway;

        public AddCustomerUseCase(ICustomerGateway customerGateway)
        {
            _customerGateway = customerGateway;
        }

        public async Task Execute(Customer customer, IErrorActionResultPresenter<ErrorDto> presenter)
        {
            await _customerGateway.Add(customer);
        }
    }
}
