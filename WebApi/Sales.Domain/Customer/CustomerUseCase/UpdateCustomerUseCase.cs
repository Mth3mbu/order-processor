﻿using System;
using System.Threading.Tasks;
using Sales.Domain.Output;

namespace Sales.Domain.Customer.CustomerUseCase
{
   public class UpdateCustomerUseCase:IUpdateCustomerUseCase
   {
       private readonly ICustomerGateway _customerGateway;

       public UpdateCustomerUseCase(ICustomerGateway customerGateway)
       {
           _customerGateway = customerGateway;
       }

       public async Task Execute(Customer request, IErrorActionResultPresenter<ErrorDto> presenter)
        {
           await _customerGateway.Update(request);
        }
    }
}
