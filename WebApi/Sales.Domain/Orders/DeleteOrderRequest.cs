﻿using System;

namespace Sales.Domain.Orders
{
    public class DeleteOrderRequest
    {
        public Guid Id { get; set; }
    }
}