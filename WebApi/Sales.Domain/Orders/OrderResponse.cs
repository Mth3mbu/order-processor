﻿using System;

namespace Sales.Domain.Orders
{
    public class OrderResponse
    {
        public Guid Id { get; set; }
        public Guid CustomerId { get; set; }
        public string Status { get; set; }
        public string PurchaseOrder { get; set; }
        public string Invoice { get; set; }
        public string DeliveryNote { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public DateTime OrderDate { get; set; }
    }
}
