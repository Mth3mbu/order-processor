﻿using System.Threading.Tasks;
using Sales.Domain.Output;
using Sales.Intergration.Sql.Orders;

namespace Sales.Domain.Orders.UseCases
{
    public class DeleteOrderUseCase : IDeleteOrderUseCase
    {
        private readonly IOrderGateway _orderGateway;

        public DeleteOrderUseCase(IOrderGateway orderGateway)
        {
            _orderGateway = orderGateway;
        }

        public async Task Execute(DeleteOrderRequest deleteAgentRequest, IErrorActionResultPresenter<ErrorDto> presenter)
        {
            await _orderGateway.DeleteOrder(deleteAgentRequest.Id);
        }
    }
}
