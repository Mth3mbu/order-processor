﻿using System;
using System.Threading.Tasks;
using Sales.Domain.Output;
using Sales.Intergration.Sql.Orders;

namespace Sales.Domain.Orders.UseCases
{
    public class GetOrderUseCase : IGetOrderUseCase
    {
        private readonly IOrderGateway _orderGateway;

        public GetOrderUseCase(IOrderGateway orderGateway)
        {
            _orderGateway = orderGateway;
        }

        public async Task Execute(Guid orderId, ISuccessOrErrorPresenter<OrderResponse, ErrorDto> presenter)
        {
            var order = await _orderGateway.GetOrder(orderId);

            presenter.Success(order);
        }
    }
}
