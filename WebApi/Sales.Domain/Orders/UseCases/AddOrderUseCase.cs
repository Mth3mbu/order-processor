﻿using System;
using System.Threading.Tasks;
using Sales.Domain.Output;
using Sales.Intergration.Sql.Orders;

namespace Sales.Domain.Orders.UseCases
{
    public class AddOrderUseCase : IAddOrderUseCase
    {
        private readonly IOrderGateway _orderGateway;

        public AddOrderUseCase(IOrderGateway orderGateway)
        {
            _orderGateway = orderGateway;
        }

        public async Task Execute(AddOrderRequest request, IErrorPresenter<ErrorDto> presenter)
        {
            await _orderGateway.AddOrder(request);
        }
    }
}
