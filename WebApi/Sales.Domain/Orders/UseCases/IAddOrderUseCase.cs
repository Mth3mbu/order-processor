﻿using System.Threading.Tasks;
using Sales.Domain.Output;

namespace Sales.Domain.Orders.UseCases
{
    public interface IAddOrderUseCase
    {
        Task Execute(AddOrderRequest request, IErrorPresenter<ErrorDto> presenter);
    }
}
