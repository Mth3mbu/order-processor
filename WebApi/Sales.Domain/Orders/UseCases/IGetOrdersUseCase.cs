﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Sales.Domain.Output;

namespace Sales.Domain.Orders.UseCases
{
    public interface IGetOrdersUseCase
    {
        Task Execute(ISuccessOrErrorPresenter<List<OrderResponse>,ErrorDto> presenter);
    }
}
