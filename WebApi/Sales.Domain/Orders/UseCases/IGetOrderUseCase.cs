﻿using System;
using System.Threading.Tasks;
using Sales.Domain.Output;

namespace Sales.Domain.Orders.UseCases
{
    public interface IGetOrderUseCase
    {
        Task Execute(Guid orderId, ISuccessOrErrorPresenter<OrderResponse, ErrorDto> presenter);
    }
}
