﻿using System.Threading.Tasks;
using Sales.Domain.Output;
using Sales.Intergration.Sql.Orders;

namespace Sales.Domain.Orders.UseCases
{
    public class UpdateOrderUseCase : IUpdateOrderUseCase
    {
        private readonly IOrderGateway _gateway;

        public UpdateOrderUseCase(IOrderGateway gateway)
        {
            _gateway = gateway;
        }

        public async Task Execute(UpdateOrderRequest request, IErrorPresenter<ErrorDto> errorPresenter)
        {
            await _gateway.UpdateOrder(request);
        }
    }
}
