﻿using System.Threading.Tasks;
using Sales.Domain.Output;

namespace Sales.Domain.Orders.UseCases
{
    public interface IDeleteOrderUseCase
    {
        Task Execute(DeleteOrderRequest deleteAgentRequest, IErrorActionResultPresenter<ErrorDto> errorPresenter);
    }
}
