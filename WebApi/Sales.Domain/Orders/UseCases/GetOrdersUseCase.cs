﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Sales.Domain.Output;
using Sales.Intergration.Sql.Orders;

namespace Sales.Domain.Orders.UseCases
{
    public class GetOrdersUseCase : IGetOrdersUseCase
    {
        private readonly IOrderGateway _orderGateway;

        public GetOrdersUseCase(IOrderGateway orderGateway)
        {
            _orderGateway = orderGateway;
        }
        public async Task Execute(ISuccessOrErrorPresenter<List<OrderResponse>, ErrorDto> presenter)
        {
            var orders = await _orderGateway.GetOrders();
            presenter.Success(orders);
        }
    }
}
