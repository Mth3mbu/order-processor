﻿using System.Threading.Tasks;
using Sales.Domain.Output;

namespace Sales.Domain.Orders.UseCases
{
    public interface IUpdateOrderUseCase
    {
        Task Execute(UpdateOrderRequest request, IErrorPresenter<ErrorDto> errorPresenter);
    }
}
