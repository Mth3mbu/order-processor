﻿using System;
using System.Collections.Generic;
using Sales.Domain.Orders;
using System.Threading.Tasks;

namespace Sales.Intergration.Sql.Orders
{
    public interface IOrderGateway
    {
        Task AddOrder(AddOrderRequest order);
        Task UpdateOrder(UpdateOrderRequest order);
        Task<List<OrderResponse>> GetOrders();
        Task<OrderResponse> GetOrder(Guid orderId);
        Task DeleteOrder(Guid orderId);
    }
}
