﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sales.Domain.Orders
{
    public interface IItemsGateway
    {
        Task<List<Item>> GetAllItemsByOrderId(Guid orderId);
        Task Delete(Guid itemId);
        Task Add(Item item);
        void AddMany(IEnumerable<Item> items);
    }
}
