﻿using System;

namespace Sales.Domain.Orders
{
    public class UpdateOrderRequest
    {
        public Guid Id { get; set; }
        public string Status { get; set; }
        public string Invoice { get; set; }
        public string PurchaseOrder { get; set; }
        public string DeliveryNote { get; set; }
    }
}