﻿namespace Sales.Domain.Output
{
    public class ErrorDto
    {
        public string Message { get; set; }
    }
}
