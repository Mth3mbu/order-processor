﻿using Microsoft.AspNetCore.Mvc;

namespace Sales.Domain.Output
{
    public interface IErrorActionResultPresenter<in TError> : IErrorPresenter<TError>
    {
        public IActionResult Render();
    }
}
