﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sales.Domain.Orders;
using Sales.Domain.Orders.UseCases;
using Sales.Domain.Output;

namespace Sales.Web.API.Orders
{
    [ApiController]
    [Route("delete-order")]
    public class DeleteOrderController : ControllerBase
    {
        public readonly IErrorActionResultPresenter<ErrorDto> _presenter;
        private readonly IDeleteOrderUseCase _deleteOrderUseCase;

        public DeleteOrderController(IDeleteOrderUseCase deleteOrderUseCase, IErrorActionResultPresenter<ErrorDto> presenter)
        {
            _deleteOrderUseCase = deleteOrderUseCase;
            _presenter = presenter;
        }

        [HttpPut]
        public async Task<IActionResult> Delete(DeleteOrderRequest request)
        {
            await _deleteOrderUseCase.Execute(request, _presenter);
            return _presenter.Render();
        }
    }
}
