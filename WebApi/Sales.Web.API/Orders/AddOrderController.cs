﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sales.Domain.Orders;
using Sales.Domain.Orders.UseCases;
using Sales.Domain.Output;

namespace Sales.Web.API.Orders
{
    [ApiController]
    [Route("place-order")]
    public class AddOrderController : ControllerBase
    {
        private readonly IErrorActionResultPresenter<ErrorDto> _errorPresenter;
        private readonly IAddOrderUseCase _addOrderUseCase;
        public AddOrderController(IErrorActionResultPresenter<ErrorDto> errorPresenter, IAddOrderUseCase addOrderUseCase)
        {
            _errorPresenter = errorPresenter;
            _addOrderUseCase = addOrderUseCase;
        }

        [HttpPost]
        public async Task<IActionResult> Add(AddOrderRequest order)
        {
            await _addOrderUseCase.Execute(order, _errorPresenter);

            return _errorPresenter.Render();
        }
    }
}
