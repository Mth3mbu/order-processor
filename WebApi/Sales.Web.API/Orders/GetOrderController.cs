﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sales.Domain.Orders;
using Sales.Domain.Orders.UseCases;
using Sales.Domain.Output;

namespace Sales.Web.API.Orders
{
    [ApiController]
    [Route("order/{orderId}")]
    public class GetOrderController : ControllerBase
    {
        private readonly ISuccessOrErrorActionResultPresenter<OrderResponse, ErrorDto> _presenter;
        private readonly IGetOrderUseCase _useCase;

        public GetOrderController(IGetOrderUseCase useCase, ISuccessOrErrorActionResultPresenter<OrderResponse, ErrorDto> presenter)
        {
            _useCase = useCase;
            _presenter = presenter;
        }

        [HttpGet]
        public async Task<IActionResult> GetOrder(Guid orderId)
        {
            await _useCase.Execute(orderId, _presenter);

            return _presenter.Render();
        }
    }
}
