﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sales.Domain.Orders;
using Sales.Domain.Orders.UseCases;
using Sales.Domain.Output;

namespace Sales.Web.API.Orders
{
    [ApiController]
    [Route("orders")]
    public class GetOrdersController : ControllerBase
    {
        private readonly IGetOrdersUseCase _getOrdersUseCase;
        private readonly ISuccessOrErrorActionResultPresenter<List<OrderResponse>, ErrorDto> _presenter;

        public GetOrdersController(ISuccessOrErrorActionResultPresenter<List<OrderResponse>, ErrorDto> presenter, IGetOrdersUseCase getOrdersUseCase)
        {
            _presenter = presenter;
            _getOrdersUseCase = getOrdersUseCase;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllOrders()
        {
            await _getOrdersUseCase.Execute(_presenter);
            return _presenter.Render();
        }
    }
}
