﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sales.Domain.Orders;
using Sales.Domain.Orders.UseCases;
using Sales.Domain.Output;

namespace Sales.Web.API.Orders
{
    [ApiController]
    [Route("edit-order")]
    public class EditOrderController : ControllerBase
    {
        private readonly IUpdateOrderUseCase _updateOrderUseCase;
        private readonly IErrorActionResultPresenter<ErrorDto> _errorPresenter;

        public EditOrderController(IUpdateOrderUseCase updateOrderUseCase, IErrorActionResultPresenter<ErrorDto> errorPresenter)
        {
            _updateOrderUseCase = updateOrderUseCase;
            _errorPresenter = errorPresenter;
        }

        [HttpPut]
        public async Task<IActionResult> Update(UpdateOrderRequest request)
        {
            await _updateOrderUseCase.Execute(request, _errorPresenter);

            return _errorPresenter.Render();
        }
    }
}
