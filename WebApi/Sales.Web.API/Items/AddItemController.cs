﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sales.Domain.Items.UseCases;
using Sales.Domain.Orders;
using Sales.Domain.Output;

namespace Sales.Web.API.Items
{
    [ApiController]
    [Route("add")]
    public class AddItemController : ControllerBase
    {
        private readonly IAddItemUseCase _useCase;
        private readonly IErrorActionResultPresenter<ErrorDto> _presenter;

        public AddItemController(IErrorActionResultPresenter<ErrorDto> presenter, IAddItemUseCase useCase)
        {
            _presenter = presenter;
            _useCase = useCase;
        }

        [HttpPost]
        public async Task<IActionResult> Add(Item request)
        {
            await _useCase.Execute(request, _presenter);
            return _presenter.Render();
        }

        [HttpPost]
        [Route("many")]
        public IActionResult Add(IEnumerable<Item> request)
        {
            _useCase.ExecuteMany(request, _presenter);
            return _presenter.Render();
        }
    }
}
