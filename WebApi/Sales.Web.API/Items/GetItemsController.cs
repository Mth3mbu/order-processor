﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sales.Domain.Items.UseCases;
using Sales.Domain.Orders;
using Sales.Domain.Output;

namespace Sales.Web.API.Items
{
    [ApiController]
    [Route("{orderId}/items")]
    public class GetItemsController : ControllerBase
    {
        private readonly ISuccessOrErrorActionResultPresenter<List<Item>, ErrorDto> _presenter;
        private readonly IGetITermsUseCase _itemCase;

        public GetItemsController(IGetITermsUseCase itemCase, ISuccessOrErrorActionResultPresenter<List<Item>, ErrorDto> presenter)
        {
            _itemCase = itemCase;
            _presenter = presenter;
        }

        [HttpGet]
        public async Task<IActionResult> GetItems(Guid orderId)
        {
            await _itemCase.Execute(orderId, _presenter);
            return _presenter.Render();
        }
    }
}
