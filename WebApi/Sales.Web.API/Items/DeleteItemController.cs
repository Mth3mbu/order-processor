﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sales.Domain.Items.UseCases;
using Sales.Domain.Output;

namespace Sales.Web.API.Items
{
    [ApiController]
    [Route("delete/{itemId}")]
    public class DeleteItemController : ControllerBase
    {
        private readonly IErrorActionResultPresenter<ErrorDto> _presenter;
        private readonly IDeleteItemUseCase _useCase;

        public DeleteItemController(IErrorActionResultPresenter<ErrorDto> presenter, IDeleteItemUseCase useCase)
        {
            _presenter = presenter;
            _useCase = useCase;
        }

        public async Task<IActionResult> DeleteItem(Guid itemId)
        {
            await _useCase.Execute(itemId, _presenter);

            return _presenter.Render();
        }
    }
}
