﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sales.Domain.Customer.CustomerUseCase;
using Sales.Domain.Output;

namespace Sales.Web.API.Customer
{
    [ApiController]
    [Route("customer/new")]
    public class AddCustomerController : ControllerBase
    {
        private readonly IAddCustomerUseCase _useCase;
        private readonly IErrorActionResultPresenter<ErrorDto> _presenter;

        public AddCustomerController(IAddCustomerUseCase useCase, IErrorActionResultPresenter<ErrorDto> presenter)
        {
            _useCase = useCase;
            _presenter = presenter;
        }

        [HttpPost]
        public async Task<IActionResult> Add(Domain.Customer.Customer request)
        {
            await _useCase.Execute(request, _presenter);

            return _presenter.Render();
        }
    }
}
