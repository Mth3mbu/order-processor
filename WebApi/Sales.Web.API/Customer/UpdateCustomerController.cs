﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sales.Domain.Customer.CustomerUseCase;
using Sales.Domain.Output;

namespace Sales.Web.API.Customer
{
    [ApiController]
    [Route("customer")]
    public class UpdateCustomerController : ControllerBase
    {
        private readonly IUpdateCustomerUseCase _useCase;
        private readonly IErrorActionResultPresenter<ErrorDto> _presenter;

        public UpdateCustomerController(IUpdateCustomerUseCase useCase, IErrorActionResultPresenter<ErrorDto> presenter)
        {
            _useCase = useCase;
            _presenter = presenter;
        }

        [HttpPut]
        public async Task<IActionResult> Update(Domain.Customer.Customer request)
        {
            await _useCase.Execute(request, _presenter);

            return _presenter.Render();
        }
    }
}
