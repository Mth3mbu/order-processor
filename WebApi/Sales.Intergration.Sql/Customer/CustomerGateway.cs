﻿using System.Data;
using System.Threading.Tasks;
using Dapper;
using Sales.Domain.Customer;
using Sales.Intergration.Sql.Connections;

namespace Sales.Intergration.Sql.Customer
{
    public class CustomerGateway : ICustomerGateway
    {
        private readonly ISalesDbConnectionContext _dbConnectionContext;

        public CustomerGateway(ISalesDbConnectionContext dbConnectionContext)
        {
            _dbConnectionContext = dbConnectionContext;
        }

        private IDbTransaction DbTransaction => _dbConnectionContext.GetTransaction();

        public async Task Add(Domain.Customer.Customer customer)
        {
            await DbTransaction.Connection.ExecuteAsync("INSERT INTO [Customer]([Id],[Name],[Address],[Phone]) VALUES(@Id,@Name,@Address,@Phone)", new
            {
                customer.Id,
                customer.Name,
                customer.Address,
                customer.Phone
            },
                DbTransaction);
        }

        public async Task Update(Domain.Customer.Customer customer)
        {
            await DbTransaction.Connection.ExecuteAsync("UPDATE [Customer] SET [Name] = @Name,[Address] = @Address WHERE[Id] = @Id", new
            {
                customer.Id,
                customer.Address,
                customer.Name,
            },
                DbTransaction);

        }
    }
}
