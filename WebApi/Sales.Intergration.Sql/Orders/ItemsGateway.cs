﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Sales.Domain.Orders;
using Sales.Intergration.Sql.Connections;

namespace Sales.Intergration.Sql.Orders
{
    public class ItemsGateway : IItemsGateway
    {
        private readonly ISalesDbConnectionContext _dbConnectionContext;

        public ItemsGateway(ISalesDbConnectionContext dbConnectionContext)
        {
            _dbConnectionContext = dbConnectionContext;
        }

        private IDbTransaction DbTransaction => _dbConnectionContext.GetTransaction();
        private IDbConnection DbConnection => _dbConnectionContext.GetConnection();
        public async Task<List<Item>> GetAllItemsByOrderId(Guid orderId)
        {
            var items = await DbConnection.QueryAsync<Item>("SELECT [Id],[Name],[Cost],[Quantity] FROM [Item] WHERE [OrderId]=@orderId", new { orderId }, DbTransaction);

            return items.ToList();
        }

        public async Task Delete(Guid itemId)
        {
            await DbTransaction.Connection.ExecuteAsync("DELETE FROM [Item] WHERE [Id]=@itemId", new { itemId },
                DbTransaction);
        }

        public async Task Add(Item item)
        {
            item.Cost = item.Cost * item.Quantity;
            await DbTransaction.Connection.ExecuteAsync("INSERT INTO [Item]([Id],[OrderId],[Name],[Cost],[Quantity]) VALUES (@Id,@OrderId,@Name,@Cost,@Quantity)", new
            {
                item.Id,
                item.OrderId,
                item.Name,
                item.Cost,
                item.Quantity
            },
                DbTransaction);
        }

        public void AddMany(IEnumerable<Item> items)
        {
            foreach (var item in items)
            {
                item.Cost = item.Cost * item.Quantity;
            }
           
            DbTransaction.Connection.Execute("INSERT INTO [Item]([Id],[OrderId],[Name],[Cost],[Quantity]) VALUES (@Id,@OrderId,@Name,@Cost,@Quantity)", items,
                DbTransaction);
        }
    }
}
