﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Sales.Domain.Orders;
using System.Threading.Tasks;
using Dapper;
using Sales.Intergration.Sql.Connections;

namespace Sales.Intergration.Sql.Orders
{
    public class OrderGateway : IOrderGateway
    {
        private readonly ISalesDbConnectionContext _dbConnectionContext;
        private IDbTransaction DbTransaction => _dbConnectionContext.GetTransaction();
        private IDbConnection DbConnection => _dbConnectionContext.GetConnection();

        public OrderGateway(ISalesDbConnectionContext dbConnectionContext)
        {
            _dbConnectionContext = dbConnectionContext;
        }

        public async Task AddOrder(AddOrderRequest order)
        {
            await DbTransaction.Connection.ExecuteAsync("INSERT INTO [Order] ([Id],[OrderDate],[Status],[IsActive],[CustomerId],[ItemId],[PurchaseOrder],[Invoice],[DeliveryNote]) " +
                                                        "VALUES(@Id, @OrderDate, @Status, @IsActive, @CustomerId, @ItemId, @PurchaseOrder, @Invoice, @DeliveryNote)",
                new
                {
                    order.Id,
                    order.Status,
                    order.Invoice,
                    order.PurchaseOrder,
                    order.DeliveryNote,
                    order.CustomerId,
                    order.ItemId,
                    OrderDate = DateTime.Now,
                    IsActive = 1
                },
                DbTransaction);
        }

        public async Task UpdateOrder(UpdateOrderRequest order)
        {
            await DbTransaction.Connection.ExecuteAsync("UPDATE [Order] SET [Status] = @Status,[PurchaseOrder]=@PurchaseOrder," +
                                                        "[Invoice]=@Invoice,[DeliveryNote]=@DeliveryNote WHERE [Id]=@Id",
                new
                {
                    order.Id,
                    order.Status,
                    order.PurchaseOrder,
                    order.Invoice,
                    order.DeliveryNote
                },
                DbTransaction);
        }

        public async Task<List<OrderResponse>> GetOrders()
        {
            var orders = await DbConnection.QueryAsync<OrderResponse>("SELECT o.[Id],[OrderDate],c.[Name] as CustomerName,[Status] FROM [Order] o " +
                                                                      "LEFT JOIN [Customer] c ON CustomerId=c.Id WHERE [IsActive]=1");

            return orders.ToList();
        }

        public async Task<OrderResponse> GetOrder(Guid orderId)
        {
            return await DbConnection.QueryFirstAsync<OrderResponse>("SELECT o.[Id],[OrderDate],[CustomerId],c.[Name] as CustomerName,c.[Address],c.[Phone],[Status],[PurchaseOrder],[Invoice],[DeliveryNote] " +
                                                                     "FROM [Order] o LEFT JOIN[Customer] c ON CustomerId = c.Id WHERE o.id =@orderId ", new { orderId }, DbTransaction);
        }

        public async Task DeleteOrder(Guid orderId)
        {
            await DbConnection.ExecuteAsync("UPDATE [Order] SET [IsActive]=0 WHERE Id=@orderId", new { orderId },
                     DbTransaction);
        }
    }
}
