﻿namespace Sales.Intergration.Sql.Connections
{
    public interface IDbSettings
    {
        string ConnectionString { get; }
    }
}
