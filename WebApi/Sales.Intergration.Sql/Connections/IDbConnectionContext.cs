﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Sales.Intergration.Sql.Connections
{
    public interface IDbConnectionContext
    {
        IDbConnection GetConnection();
        IDbTransaction GetTransaction();
        void Commit();
        void Rollback();
    }
}
