﻿using System;
using FluentMigrator.Runner;
using Microsoft.Extensions.DependencyInjection;

namespace Sales.Intergration.Sql.Migrations
{
    public class MigrationRunner
    {
        public void Migrate(string connectionString)
        {
            var serviceProvider = (IServiceProvider)new ServiceCollection()
                .AddFluentMigratorCore()
                .ConfigureRunner(rb => rb
                    .AddSqlServer()
                    .WithGlobalConnectionString(connectionString)
                    .ScanIn(typeof(MigrationRunner).Assembly)
                    .For
                    .All()
                )
                .AddLogging(lb => lb.AddFluentMigratorConsole())
                .BuildServiceProvider(false);

            using var scope = serviceProvider.CreateScope();
            scope.ServiceProvider
                .GetRequiredService<IMigrationRunner>()
                .MigrateUp();
        }
    }
}
