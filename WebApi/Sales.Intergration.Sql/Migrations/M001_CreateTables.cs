﻿using System;
using FluentMigrator;

namespace Sales.Intergration.Sql.Migrations
{
    [Migration(202001261812)]
    public class M001_CreateTables : Migration
    {
        public override void Up()
        {
            Create.Table("Customer")
                .WithColumn("Id").AsGuid().PrimaryKey()
                .WithColumn("Name").AsString()
                .WithColumn("Address").AsString()
                .WithColumn("Phone").AsString(10);

            Create.Table("Order")
                .WithColumn("Id")
                .AsGuid().PrimaryKey()
                .WithColumn("OrderDate").AsDateTime()
                .WithColumn("Status").AsString()
                .WithColumn("IsActive").AsBoolean()
                .WithColumn("CustomerId").AsGuid()
                .ForeignKey("Fk_CustomerId", "Customer", "Id")
                .WithColumn("ItemId").AsGuid()
                .WithColumn("PurchaseOrder").AsString(Int32.MaxValue)
                .WithColumn("Invoice").AsString(Int32.MaxValue)
                .WithColumn("DeliveryNote").AsString(Int32.MaxValue);

            Create.Table("Item")
                .WithColumn("Id")
                .AsGuid().PrimaryKey()
                .WithColumn("OrderId").AsGuid().ForeignKey("fk_orderId", "Order", "Id")
                .PrimaryKey().WithColumn("Name").AsString()
                .WithColumn("Cost").AsDecimal()
                .WithColumn("Quantity").AsInt32();
        }

        public override void Down()
        {

        }
    }
}
