﻿using FluentMigrator;

namespace Sales.Intergration.Sql.Migrations
{
    [Migration(202001271309)]
    public class M003_CreateSeed : Migration
    {
        public override void Up()
        {
            Insert.IntoTable("Item").Row(new
            {
                Id = "e9c7013a-a3f7-482c-9a75-8243bda45c77",
                OrderId = "b700bcc9-e423-4ff3-b1df-1cad4d5a3f9e",
                Name = "Mc Flury",
                Cost = 52.45,
                Quantity = 1
            });

            Insert.IntoTable("Item").Row(new
            {
                Id = "86fabac7-f211-4d10-aa23-d189f7a48bd6",
                OrderId = "b700bcc9-e423-4ff3-b1df-1cad4d5a3f9e",
                Name = "Hot Wings",
                Cost = 78.45,
                Quantity = 1
            });
        }

        public override void Down()
        {

        }
    }
}
