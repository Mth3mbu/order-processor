﻿using System;
using FluentMigrator;

namespace Sales.Intergration.Sql.Migrations
{
    [Migration(202001261919)]
    public class M002_CreateSeed : Migration
    {
        public override void Up()
        {
            Insert.IntoTable("Customer").Row(new
            {
                Id = "f0876e5d-99a3-4179-aba2-b3a7aba69b19",
                Name = "Bradley Vause",
                Address = "6 Birkenhead Umbilo",
                Phone = "0735687788"
            });

            Insert.IntoTable("Customer").Row(new
            {
                Id = "0d034ac7-4b39-415a-a5f3-073745b0dedd",
                Name = "Brendon Page",
                Address = "56 Mangosuthu High way",
                Phone = "0789638521"
            });

            Insert.IntoTable("Order").Row(new
            {
                Id = "b700bcc9-e423-4ff3-b1df-1cad4d5a3f9e",
                OrderDate = DateTime.Now,
                ItemId = "c4c7f53f-2808-4ea6-9a48-032a524da75e",
                CustomerId = "f0876e5d-99a3-4179-aba2-b3a7aba69b19",
                Status = "New",
                IsActive = 1,
                PurchaseOrder = "",
                Invoice = "",
                DeliveryNote = ""
            });

            Insert.IntoTable("Order").Row(new
            {
                Id = "9fbd4ff8-5ee6-447e-97aa-de85f4d42d2a",
                OrderDate = DateTime.Now,
                ItemId = "5e555867-6371-4b42-a9f2-9e6e72e9a96a",
                CustomerId = "0d034ac7-4b39-415a-a5f3-073745b0dedd",
                Status = "New",
                IsActive = 1,
                PurchaseOrder = "",
                Invoice = "",
                DeliveryNote = ""
            });

            Insert.IntoTable("Item").Row(new
            {
                Id = "c4c7f53f-2808-4ea6-9a48-032a524da75e",
                OrderId = "b700bcc9-e423-4ff3-b1df-1cad4d5a3f9e",
                Name = "Mc Flury",
                Cost = 52.45,
                Quantity = 1
            });

            Insert.IntoTable("Item").Row(new
            {
                Id = "5e555867-6371-4b42-a9f2-9e6e72e9a96a",
                OrderId = "9fbd4ff8-5ee6-447e-97aa-de85f4d42d2a",
                Name = "Hot Wings",
                Cost = 78.45,
                Quantity = 1
            });

        }

        public override void Down()
        {

        }
    }
}
