﻿using System;
using Microsoft.Extensions.Configuration;
using Sales.Intergration.Sql.Connections;

namespace Sales.Web.Main.Infrastructure
{
    public class DbSettings: IDbSettings
    {
        private readonly IConfiguration _configuration;

        public DbSettings(IConfiguration configuration)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        public string ConnectionString => _configuration.GetConnectionString("salesDb");
    }
}
