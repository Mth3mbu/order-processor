﻿namespace Sales.Web.Main.Infrastructure
{
    public class CorsSettings
    {
        public string[] Origins { get; set; }
    }
}
