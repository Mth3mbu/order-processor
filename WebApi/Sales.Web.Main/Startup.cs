using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Sales.Intergration.Sql.Connections;
using Sales.Intergration.Sql.Migrations;
using Sales.Web.API.Orders;
using Sales.Web.Main.Infrastructure;
using Sales.Web.Main.IoCConfigs;
using Sales.Web.Main.Middleware;

namespace Sales.Web.Main
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOrder()
                .AddItems()
                .AddCustomer()
                .AddPresenters()
                .AddSqlIntegration()
                .AddControllers()
                .AddApplicationPart(typeof(GetOrdersController).Assembly)
                .AddNewtonsoftJson();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseMiddleware<EnableRequestBodyRereadMiddleware>();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            var corsSettings = Configuration.Read<CorsSettings>("Cors");
            app.UseCors(builder => builder
                .WithOrigins(corsSettings.Origins)
                .AllowAnyHeader()
                .AllowAnyMethod()
            );

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            RunDbMigrations(app);
        }

        private void RunDbMigrations(IApplicationBuilder app)
        {
            using var startupScope = app.ApplicationServices.CreateScope();
            var dbSettings = startupScope.ServiceProvider.GetService<IDbSettings>();
            new MigrationRunner().Migrate(dbSettings.ConnectionString);
        }
    }
}
