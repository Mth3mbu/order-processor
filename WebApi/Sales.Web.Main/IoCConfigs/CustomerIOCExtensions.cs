﻿using Microsoft.Extensions.DependencyInjection;
using Sales.Domain.Customer;
using Sales.Domain.Customer.CustomerUseCase;
using Sales.Intergration.Sql.Customer;

namespace Sales.Web.Main.IoCConfigs
{
    public static class CustomerIOCExtensions
    {
        public static IServiceCollection AddCustomer(this IServiceCollection services)
        {
            services.AddScoped<ICustomerGateway, CustomerGateway>();
            services.AddScoped<IAddCustomerUseCase, AddCustomerUseCase>();
            services.AddScoped<IUpdateCustomerUseCase, UpdateCustomerUseCase>();
            return services;
        }
    }
}
