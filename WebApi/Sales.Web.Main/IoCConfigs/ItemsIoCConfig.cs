﻿using Microsoft.Extensions.DependencyInjection;
using Sales.Domain.Items.UseCases;
using Sales.Domain.Orders;
using Sales.Intergration.Sql.Orders;

namespace Sales.Web.Main.IoCConfigs
{
    public static class ItemsIoCConfig
    {
        public static IServiceCollection AddItems(this IServiceCollection services)
        {
            services.AddScoped<IItemsGateway, ItemsGateway>();
            services.AddScoped<IGetITermsUseCase, GetItemsUseCase>();
            services.AddScoped<IDeleteItemUseCase, DeleteItemUseCase>();

            return services;
        }
    }
}
