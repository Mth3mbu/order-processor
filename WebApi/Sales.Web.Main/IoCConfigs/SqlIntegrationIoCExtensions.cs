﻿using Microsoft.Extensions.DependencyInjection;
using Sales.Intergration.Sql.Connections;
using Sales.Web.Main.Infrastructure;

namespace Sales.Web.Main.IoCConfigs
{
    public static class SqlIntegrationIoCExtensions
    {
        public static IServiceCollection AddSqlIntegration(this IServiceCollection services)
        {
            services.AddScoped<IDbSettings, DbSettings>();
            services.AddScoped<ISalesDbConnectionContext>(
                provider => new AutoConnectingDbConnectionContext(provider.GetService<IDbSettings>(), settings => settings.ConnectionString)
            );
           
            return services;
        }
    }
}
