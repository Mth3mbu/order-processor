﻿using Microsoft.Extensions.DependencyInjection;
using Sales.Domain.Items.UseCases;
using Sales.Domain.Orders.UseCases;
using Sales.Intergration.Sql.Orders;

namespace Sales.Web.Main.IoCConfigs
{
    public static class OrderIoCExtensions
    {
        public static IServiceCollection AddOrder(this IServiceCollection services)
        {
            services.AddScoped<IAddOrderUseCase, AddOrderUseCase>();
            services.AddScoped<IUpdateOrderUseCase, UpdateOrderUseCase>();
            services.AddScoped<IDeleteOrderUseCase, DeleteOrderUseCase>();
            services.AddScoped<IGetOrderUseCase, GetOrderUseCase>();
            services.AddScoped<IGetOrdersUseCase, GetOrdersUseCase>();
            services.AddScoped<IOrderGateway, OrderGateway>();
            services.AddScoped<IAddItemUseCase, AddITermUseCase>();

            return services;
        }
    }
}
