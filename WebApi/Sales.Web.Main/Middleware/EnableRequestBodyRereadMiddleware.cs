﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Sales.Web.Main.Middleware
{
    public class EnableRequestBodyRereadMiddleware
    {
        private RequestDelegate _next;

        public EnableRequestBodyRereadMiddleware(RequestDelegate next)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
        }

        public async Task Invoke(HttpContext context)
        {
            context.Request.EnableBuffering();
            await _next(context);
        }
    }
}
